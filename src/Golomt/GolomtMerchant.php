<?php


namespace TseMerchant\Golomt;


use GuzzleHttp\Client;
use Psr\Log\LoggerInterface;

class GolomtMerchant
{
    private const CARD_INFO_URL = 'https://m.egolomt.mn/billingnew/cardinfo.aspx';
    private const WSDL = 'https://m.egolomt.mn:7073/persistence.asmx?WSDL';
    public const LANG_MONGOLIAN = 0;
    public const LANG_ENGLISH = 1;

    private $isProd;
    private $baseUri;
    private $client;

    private $soapUsername;
    private $soapPassword;

    private $key;

    /**
     * @var LoggerInterface|null
     */
    private $logger = null;

    public function __construct($options)
    {
        $this->isProd = isset($options['env']) && $options['env'] === 'prod';

        $this->options = array_merge([
        ], $options['params'] ?? []);

        $this->logger = $options['logger'] ?? null;

        $this->soapUsername = $options['soapUsername'] ?? null;
        $this->soapPassword = $options['soapPassword'] ?? null;
        $this->key = $options['key'] ?? null;

        $this->client = new \SoapClient(self::WSDL);
    }

    public function generateForm($transNumber, $transAmount, $options = array())
    {
        $cardInfoUrl = self::CARD_INFO_URL;

        $formValues = [
            'key_number' => $this->key,
            'trans_number' => $transNumber,
            'trans_amount' => $transAmount,
            'lang_ind' => $options['lang'] ?? self::LANG_MONGOLIAN,
        ];

        $id = $options['id'] ?? 'golomt-form';

        $formElements = [<<<HTML
            <form id="${id}" action="${cardInfoUrl}" method="post">
HTML
        ];

        $optionalKeys = ['color', 'social', 'time', 'subID', 'signature'];

        foreach ($optionalKeys as $optionalKey)
        {
            if(isset($options[$optionalKey])) {
                $formValues[$optionalKey] = $options[$optionalKey];
            }
        }

        foreach($formValues as $formKey => $formValue)
        {
            $formElements[] = <<<HTML
                <input type="text" name="$formKey" value="$formValue" />
HTML;
        }

        $formElements[] = '</form>';

        return \implode('', $formElements);

    }

    public function getResult($transNumber, $transDate, $amount)
    {
        $result =  $this->client->Get_new([
            'v0' => $this->soapUsername,
            'v1' => $this->soapPassword,
            'v2' => $transNumber,
            'v3' => $transDate,
            'v4' => $amount,
        ]);

        $responseCode = $result['Get_newResult'] ?? null;

        if(\strlen($responseCode) === 6) {
            return [
                'status' => 'success',
                'message' => 'success',
                'result' => $result,
            ];
        }

        return [
            'status' => 'fail',
            'message' => $this->getMessage($responseCode),
            'result' => $result,
        ];
    }

    private function getMessage($responseCode)
    {
        $errormsg = 'unknown';

        if ($responseCode == 0) {
            $errormsg = "golomt - soap responseCode = 0 - Өмнө нь хийгдэж байгаагүй гүйлгээ. Гүйлгээний дугаар буруу явах эсвэл өмнө нь ийм дугаартай гүйлгээ хийгдэж байгаагүй тохиолдолд";
//                echo "Iim dugaar bolon guilgeenii duntei guilgee baazad burtgegdeegui baina.<br>";
        } else if ($responseCode == 2) {
            $errormsg = "golomt - soap responseCode = 2 - Гүйлгээ амжилтгүй болсон. Энэ тохиолдолд хэрэглэгчийн картын мэдээлэл бууруу, эсвэл үлдэгдэл хүрэхгүй байх тохиолдлуудад";
//                echo "Guilgee amjiltgui bolson baina.<br>";
        } else if ($responseCode == 3) {
            $errormsg = "golomt - soap responseCode = 3 - Login буруу. Тохиолдолд user_id, password-оо шалгах хэрэгтэй";
//                echo "Hereglegchiin ner esvel nuuts ug buruu baina.<br>";
        } else if ($responseCode == 4) {
            $errormsg = "golomt - soap responseCode = 4 - SOAP-руу дамжуулах утгууд буруу илгээгдсэн. Жишээ: ognoo формат буруу байх , amount хоосон илгээх, буруу amount илгээх, зэрэг тохиолдлууд.";
//                echo "Hereglegchiin ner esvel nuuts ug buruu baina.<br>";
        } else if ($responseCode == '300-05') {
            $errormsg = "golomt - soap responseCode = 300-05 - Гүйлгээ хийх эрхгүй карт";
        } else if ($responseCode == '300-12') {
            $errormsg = "golomt - soap responseCode = 300-12 - Картын мэдээлэл буруу";
        } else if ($responseCode == '300-14') {
            $errormsg = "golomt - soap responseCode = 300-14 - Ийм карт байхгүй байна";
        } else if ($responseCode == '300-51') {
            $errormsg = "golomt - soap responseCode = 300-51 - Vлдэгдэл хvрэхгvй";
        } else if ($responseCode == '300-54') {
            $errormsg = "golomt - soap responseCode = 300-54 - Картын хугацаа дууссан/Буруу оруулсан";
        } else if ($responseCode == '300-58') {
            $errormsg = "golomt - soap responseCode = 300-58 - Зөвшөөрөгдөөгүй гүйлгээ байна";
        } else if ($responseCode == '300-89') {
            $errormsg = "golomt - soap responseCode = 300-89 - Алдаатай терминал";
        } else if ($responseCode == '902-91') {
            $errormsg = "golomt - soap responseCode = 902-91 - TIMEOUT";
        } else if ($responseCode == '902-96') {
            $errormsg = "golomt - soap responseCode = 902-96 - System error";
        } else {
            $errormsg = "golomt - responseCode = " . $errormsg . " - Мэдэхгүй код";
        }

        return $errormsg;
    }

    private function writeLog($level, $message, $context = [])
    {
        if($this->logger) {
            $this->logger->log($level, $message, $context);
        }
        else {
            var_dump($level, $message, $context);
        }
    }


}