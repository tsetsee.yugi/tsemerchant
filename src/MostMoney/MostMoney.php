<?php

namespace TseMerchant\MostMoney;

use phpseclib\Crypt\AES;
use phpseclib\Crypt\RSA;
use Psr\Log\LoggerInterface;
use RobRichards\XMLSecLibs\XMLSecurityDSig;

class MostMoney
{
    private const BASE_PROD = 'https://webpos.merchant.mn/ots/api/mapi/';
    private const BASE_DEV = 'http://202.131.242.165:9089/api/mapi/';

    private $client;
    private $options;
    private $isProd;
    /**
     * @var LoggerInterface|null
     */
    private $logger = null;

    private $headers = [];
    private $baseUri;

    public function __construct($options)
    {
        $this->isProd = isset($options['env']) && $options['env'] === 'prod';

        $this->options = array_merge([
            'tranCur' => 'MNT',
            'qrColor' => '#FF0000',
            'qrSize' => '180',
            'qrPaidLimit' => '0',
            'lang' => '0',
            'deviceIp' => '',
            'deviceMac' => '',
            'deviceName' => '',
        ], $options['params'] ?? []);

        $this->headers = [
            'Content-Type: application/json; charset=utf-8',
            'Accept: application/json',
            'Accept-Charset: utf-8',
            'Keep-Alive: false',
            'Use-Default-Credentials: true',
            'PV: 05',
            'RS: 00'
        ];

        $this->logger = $options['logger'] ?? null;

        $this->baseUri = $options['baseUri'] ?? ($this->isProd ? self::BASE_PROD : self::BASE_DEV);
    }

    private function writeLog($level, $message, $context = [])
    {
        if($this->logger) {
            $this->logger->log($level, $message, $context);
        }
        else {
            var_dump($level, $message, $context);
        }
    }

    private function handleRequest($method, $uri, $options = [])
    {
        $headers = \array_merge($this->headers, $options['headers'] ?? []);

        if(isset($options['json'])) {
            $options['body'] = \json_encode($options['json']);
        }

        if($method !== 'GET') {
            $headers[] = 'Content-Length: ' . \strlen($options['body'] ?? '');
        }
        // var_dump($params);

        $this->writeLog('debug', 'MostMoney-Request-Start', [
            'uri' => $this->baseUri . $uri,
            'method' => $method,
            'headers' => $headers,
            'body' => $options['body'] ?? null,
            'options' => $options,
        ]);

        $ch = curl_init($this->baseUri . $uri);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);

        if($method !== 'GET') {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $options['body']);
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//        curl_setopt($ch, CURLOPT_VERBOSE, true);
        $result = curl_exec($ch);

        $this->writeLog('debug', 'MostMoney-Request-End', [
            'uri' => $this->baseUri . $uri,
            'method' => $method,
            'headers' => $headers,
            'body' => $options['body'] ?? null,
            'options' => $options,
            'result' => $result,
            'curl_error' => curl_error($ch),
        ]);

        curl_close($ch);

        return \json_decode($result, true);
    }

    public function createQR($amount, $currency, $desc = '', $options = [])
    {
        $traceNo = $this->createTraceNo();

        $response = $this->handleRequest('POST', 'TT3051', [
            'headers' => [
                'TT: 3051'
            ],
            'json' => \array_merge($this->getOptionByKeys([
                'payeeId',
                'posNo',
                'qrColor',
                'qrSize',
                'qrPaidLimit',
                'channel',
                'lang',
                'deviceIp',
                'deviceMac',
                'deviceName']),
                $this->getSecurityKeys($traceNo), [
                    'traceNo' => $traceNo,
                    'tranAmount' => $amount,
                    'tranCur' => $currency ?? 'MNT',
                    'tranDesc' => $desc,
                ], $options),
        ]);

        if(isset($response['responseCode']) && $response['responseCode'] === '0') {
            $response['responseData'] = \json_decode($response['responseData'], true);
        }

        return $response;
    }

    public function checkQR($qrCode)
    {
        $traceNo = $this->createTraceNo();
        $response = $this->handleRequest('POST', 'TT3065', [
            'headers' => [
                'TT: 3065',
            ],
            'json' => \array_merge($this->getOptionByKeys([
                'srcInstId',
                'channel',
                'lang',
                'posNo',
                'payeeId'
            ]), $this->getSecurityKeys($traceNo), [
                'traceNo' => $traceNo,
                'qrCode' => $qrCode,
                'isCheckQr' => '1',
            ])
        ]);

        if($response['responseCode'] === '0') {
            $response['responseData'] = \json_decode($response['responseData'], true);
        }

        return $response;
    }

    public function cancelQR($qrCode)
    {
        $traceNo = $this->createTraceNo();
        $response = $this->handleRequest('POST', 'TT3065', [
            'headers' => [
                'TT: 3065',
            ],
            'json' => \array_merge($this->getOptionByKeys([
                'srcInstId',
                'channel',
                'lang',
                'posNo',
                'payeeId'
            ]), $this->getSecurityKeys($traceNo), [
                'traceNo' => $traceNo,
                'qrCode' => $qrCode,
                'isCheckQr' => '0',
            ])
        ]);

        return $response;
    }

    public function checkBatchQR($qrCodes)
    {
        $traceNo = $this->createTraceNo();
        $response = $this->handleRequest('POST', 'TT3066', [
            'headers' => [
                'TT: 3066',
            ],
            'json' => \array_merge($this->getOptionByKeys([
                'srcInstId',
                'channel',
                'lang',
                'posNo',
                'payeeId'
            ]), $this->getSecurityKeys($traceNo), [
                'traceNo' => $traceNo,
                'qrCode' => \implode(',', $qrCodes),
                'isCheckQr' => '2',
            ])
        ]);

        return $response;
    }

    public function cancelBatchQR($qrCodes)
    {
        $traceNo = $this->createTraceNo();
        $response = $this->handleRequest('POST', 'TT3067', [
            'headers' => [
                'TT: 3067',
            ],
            'json' => \array_merge($this->getOptionByKeys([
                'srcInstId',
                'channel',
                'lang',
                'posNo',
                'payeeId'
            ]), $this->getSecurityKeys($traceNo), [
                'traceNo' => $traceNo,
                'qrCode' => \implode(',', $qrCodes),
            ])
        ]);

        return $response;
    }

    public function handleResult($xml) {
        $doc = new \DOMDocument();
        $doc->loadXML($xml);

        $xmlDsig = $this->validateResult($doc);

        if(!$xmlDsig) {
            return null;
        }

        return [
            'retType' => $doc->getElementsByTagName('retType')[0]->nodeValue,
            'retDesc' => $doc->getElementsByTagName('retDesc')[0]->nodeValue,
            'paymentId' => $doc->getElementsByTagName('paymentId')[0]->nodeValue,
            'bankTxnId' => $doc->getElementsByTagName('bankTxnId')[0]->nodeValue,
            'bankTxnDate' => $doc->getElementsByTagName('bankTxnDate')[0]->nodeValue,
            'amount' => $doc->getElementsByTagName('amount')[0]->nodeValue,
            'billId' => $doc->getElementsByTagName('billId')[0]->nodeValue,
        ];
    }


//    private function handleRequest(ResponseInterface $response)
//    {
//        return \json_decode((string)$response->getBody(), true);
//    }

    private function getSecurityKeys($traceNo)
    {
        $data = '{"traceNo":"'.$traceNo.'"}';

        return [
            'SD' => $this->encrypt1($traceNo),
            'EK' => $this->encrypt2($traceNo),
            'SG' => $this->hashdata($data),
        ];
    }

    private function createTraceNo()
    {
        return \date('YmdHis') . \substr(\microtime(), 2, 2);
    }

    private function getOptionByKeys($keys)
    {
        $option = [];

        foreach ($keys as $key) {
            $option[$key] = $this->options[$key] ?? null;
        }

        return $option;
    }

    private function encrypt1($key)
    {
        $data = '{"traceNo":"'.$key.'"}';
        $cipher = new AES();
        $cipher->setKey($key);
        $iv=pack('C*', 0xA1, 0xE2, 0xD5, 0xFE, 0xDA, 0x52, 0x0A, 0x8F, 0x8A, 0x19, 0xAA, 0xBB, 0x0A, 0xD0, 0x55, 0xAC);
        $cipher->setIV($iv);
        return \base64_encode($cipher->encrypt($this->toByte($data)));
//        return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, hash('SHA256', $key, true), $data, MCRYPT_MODE_ECB));
    }

    private function encrypt2($key)
    {
        $rsa = new RSA();
        $rsa->loadKey(file_get_contents(__DIR__ . '/cert/cert.pem'));
        $rsa->setEncryptionMode(RSA::ENCRYPTION_PKCS1);
        $rsa->setPublicKey();

        $ciphertext = $rsa->encrypt($key);
        return base64_encode($ciphertext);
    }

    private function hashdata($data){
        return base64_encode(sha1($this->toByte($data), true));
    }

    private function toByte($data){
        $bytes=unpack('C*', $data);
        $byte = '';
        foreach($bytes as $k => $v) {$byte.= chr($v & 0xFF);chr($v & 0xFF).' ';}
        return  $byte;
    }

    private function validateResult($doc)
    {
        $objDSig = new XMLSecurityDSig();

        try {
            $objDSig->locateSignature($doc);
            $objDSig->canonicalizeSignedInfo();
            $objDSig->setCanonicalMethod(XMLSecurityDSig::C14N);
//            $objDSig->addReference($doc, XMLSecurityDSig::SHA1, array('http://www.w3.org/2000/09/xmldsig#enveloped-signature'), array('force_uri' => TRUE));

            $docValid = $objDSig->validateReference();

            $objKey = $objDSig->locateKey();
            if ($objKey) {
                $this->writeLog('debug', 'objkey found', [
                    'doc' => $doc,
                ]);
                $objKey->loadKey(__DIR__ . '/cert/server.crt', true, false);
                $docValid = $objDSig->verify($objKey) === 1;
                $this->writeLog('debug', 'doc valid?', [
                    'docValid' => $docValid,
                    'doc' => $doc,
                ]);
            }
            else {
                $this->writeLog('debug', 'objkey not found', [
                    'doc' => $doc,
                ]);
            }
        }
        catch (\Exception $e) {
           $docValid = false;
           $this->writeLog('critical', 'exception', [
               'e' => $e,
               'trace' => $e->getTrace(),
           ]);
        }

        return $docValid ? $objDSig : null;
    }
}