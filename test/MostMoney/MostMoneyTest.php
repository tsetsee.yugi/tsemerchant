<?php

namespace TseMerchant\Test\MostMoney;

use PHPUnit\Framework\TestCase;
use TseMerchant\MostMoney\MostMoney;

class MostMoneyTest extends TestCase
{
    public function test()
    {
        $mostMoney = new MostMoney([
            'params' => [
                'posNo' => '60883',
                'payeeId' => '60883',
                'srcIntId' => '201908',
                'channel' => '44',
            ]
        ]);

        $result = $mostMoney->createQR(30010, 'MNT', 'TestTest');
        print_r('---------------' . PHP_EOL);
        print_r($result);
        $this->assertEquals($result['responseCode'], 0, 'Response Code нь 0 ирэх ёстой');

//        $check = $mostMoney->checkQR($result['responseData']['qr_code']);

//        print_r($check);

//        $this->assertEquals($check['responseCode'], 0, 'Response Code нь 0 ирэх ёстой');

//        $d = $mostMoney->decrypt1('PbweNftqV2VCggD2ShjKI//uWaGHVBtk3jiFPGtWIuU=');
//
//        print_r($d);
//
//        $d2 = $mostMoney->decrypt2('O0YmtEvZ9PTanP1bTNl5s4HxAh+SADQeVZDcqvAR7+rUkVrde2p5JabLeeR6RcFSfp3EG7K62thzr2u3hYj1qyP9p2aAotJFsnqGZIw6birOVL/wbmtkkKKeVNp/vpyqTiEqtAMSB/e3kj6il5CArO6xkVMoGfkqF2L307k+c1o=');
//
//        print_r($d2);

//        $result = $mostMoney->handleResult(htmlspecialchars_decode(file_get_contents(__DIR__ . '/files/input.tse.xml')));
//        var_dump($result);

//        $this->assertNotEmpty($result, 'not empty');
        return null;
    }
}